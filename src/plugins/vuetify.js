import Vue from "vue";
import "@mdi/font/css/materialdesignicons.css";
import Vuetify from "vuetify/lib";
// import colors from "vuetify/lib/util/colors";
Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    // themes: {
    //   light: {
    //     primary: colors.red.darken1, // #E53935
    //     secondary: colors.red.lighten4, // #FFCDD2
    //     accent: colors.indigo.base, // #3F51B5
    //   },
    // },
  },
  icons: {
    iconfont: ["mdi"],
    values: {
      starFull: "mdi-star",
      starHalf: "mdi-half",
      check: "mdi-check",
      clock: "mdi-clock",
      starBlank: "mdi-star-outline",
    }, // default - only for display purposes
  },
});
