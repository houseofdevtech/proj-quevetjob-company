export const RegisterPersonal2Mixin = {
  data() {
    return {
      detail: {
        company_id: localStorage.idUser,
        sub_com_id: null,
        address_com: {
          company_name: null,
          no: null,
          sign: null,
          postcode: null,
          province: null,
          district: null,
          zone: null,
          phone: null,
          note: null,
        },
        location: {
          latitude: 0.0000,
          longtitude:0.000,
        },
        tools: {
          cbc: false,
          blood: false,
          x_ray: false,
          ultrasound: false,
          anesthesia: false,
          note_tool: null,
        },
        cost: {
          min_cost: null,
          max_cost: null,
        },
        time: {
          start: null,
          end: null,
        },
      },
      name: null,

      nothing: null,
      // human: [],
      checkcom: false,
      time1: null,
      time2: null,
    };
  },

  methods: {
    check() {
      console.log(this.detail.tools.cbc);
    },
    go_registerPer() {
      this.$router.push("/registerpersonal");
    },
    go_usermanage() {
      this.$router.push("/usermanage");
    },
    go_back() {
      window.history.back();
    },

    check1() {
      console.log(this.detail.cost.max_cost);
      console.log(this.detail.cost.min_cost);
    },
  },
  created() {
    console.log(this.idUser);
  },
};
