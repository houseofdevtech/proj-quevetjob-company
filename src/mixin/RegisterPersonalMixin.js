import BackGround from "../components/Background";
import GreyPage from "../components/GreyPage";
import Address from "../components/Address";
import AddressNow from "../components/AddressNow";

export const RegisterPersonalMixin = {
  data() {
    return {
      ///array dropdown position
      register: {
        company_id: null,
        sub_com_id:null,
        position: null,
        company_name: null,
        title: null,
        name: null,
        email: null,
        phone: null,
      },
    };
  },
  methods: {
    changePage() {
      this.$router.push("/registerpersonal2");
    },
    go_back() {
      window.history.back();
    },

    addressSameplace() {
      var address_too = document.getElementById("address_too");
      if (address_too.checked == true) {
        this.profile.sameAddress = "checked";
        // console.log(this.postcode);
        // console.log(this.district);
        // console.log(this.province);
        // console.log(this.zone);

        this.profile.addressNow.noNow = this.profile.address.no;
        this.profile.addressNow.postcodeNow = this.profile.address.postcode;
        this.profile.addressNow.provinceNow = this.profile.address.province;
        this.profile.addressNow.districtNow = this.profile.address.district;
        this.profile.addressNow.zoneNow = this.profile.address.zone;
        console.log(this.sameAddress);
      } else {
        this.sameAddress = "unchecked";

        console.log(this.sameAddress);
      }
    },
  },

  components: {
    BackGround,
    GreyPage,
    AddressNow,
    Address,
  },
};
