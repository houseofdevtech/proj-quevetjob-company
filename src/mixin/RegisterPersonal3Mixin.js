import BackGround from "../components/Background";
import GreyPage from "../components/GreyPage";

export const RegisterPersonal3Mixin = {
  data() {
    return {
      ansQ: null,
      noteQ: null,
      commentQ: null,

      checkquestion: null,
      idUser: localStorage.idUser,
      idline: localStorage.idline,
    };
  },
  methods: {
    go_usermanage() {
      this.$router.push("/usermanage");
    },
    go_registerPer2() {
      this.$router.push("/registerpersonal2");
    },
    go_education() {
      this.$router.push("/education");
    },
  },
  // created() {
  //   this.get_question();
  //   // console.log("ok");
  // },
  components: {
    BackGround,
    GreyPage,
  },
};
