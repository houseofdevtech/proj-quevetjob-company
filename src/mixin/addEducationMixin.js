import BackGround from "../components/Background";

export const addEducationMixin = {
  methods: {
    go_usermanage() {
      this.$router.push("/usermanage");
    },
    go_education() {
      this.$router.push("/education");
    },
  },
  components: {
    BackGround,
  },
};
