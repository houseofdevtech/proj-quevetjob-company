import BackGround from "../components/Background";
import GreyPage from "../components/GreyPage";
 import Datepicker from "vuejs-datepicker";
export const ExperienceMixin = {
  data() {
    return {
      oldPosition: null,
      company: null,
      province: null,
      startDay: null,
      finishDay: null,
      idUser: localStorage.idUser,
      idLine: localStorage.idLine,
      checkexp: false,
    };
  },
  methods: {
    go_addexperience() {
      console.log(this.startDay);
      console.log(this.finishDay);

      this.$router.push("/addexperience");
    },
  },
  components: {
    BackGround,
    GreyPage,
    Datepicker,
  },
};
