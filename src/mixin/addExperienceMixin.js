import BackGround from "../components/Background";

export const addExperienceMixin = {
  methods: {
    go_usermanage() {
      this.$router.push("/usermanage");
    },
    go_experience() {
      this.$router.push("/experience");
    },
  },
  components: {
    BackGround,
  },
};
