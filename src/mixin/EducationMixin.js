import BackGround from "../components/Background";
import GreyPage from "../components/GreyPage";

export const EducationMixin = {
  data() {
    return {
      levelEducation: null,
      university: null,
      branch: null,
      graduation: null,
      educationStatus: null,
      gpa: null,
      idUser: localStorage.idUser,
      idLine: localStorage.idLine,
      checkedu: false,
    };
  },
  methods: {
    go_addeducation() {
      this.$router.push("/addeducation");
    },
  },
  components: {
    BackGround,
    GreyPage,
  },
};
