import axios from "axios";

const baseURL = "https://vulcan.houseofdev.tech/qvetjob";

export default axios.create({
  baseURL
});
