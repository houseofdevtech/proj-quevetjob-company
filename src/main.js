import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import lodash from "lodash";
import VueLodash from "vue-lodash";
import axios from "axios";
import VueMeta from "vue-meta";
import vuetify from './plugins/vuetify';

Vue.use(VueLodash, { name: "custom", lodash: lodash });
Vue.use(VueMeta);
// import VuejsDialog from "vuejs-dialog";
// import VuejsDialogMixin from "vuejs-dialog/dist/vuejs-dialog-mixin.min.js"; // only needed in custom components

// // include the default style
// import "vuejs-dialog/dist/vuejs-dialog.min.css";

// // Tell Vue to install the plugin.
// Vue.use(VuejsDialog);
import VCalendar from "v-calendar";
Vue.use(VCalendar, {
  componentPrefix: 'vc',
  themeStyles: {
wrapper: {
background: 'linear-gradient(to bottom , #0d47a1, #6596e0)',
color: 'white'
},
weekdays: {
padding: '20px' ,
},
weeks: {
padding: '20px',
},
dayContent: {
padding: '30px',
}
}  // Use <vc-calendar /> instead of <v-calendar />
});
Vue.config.productionTip = false;
new Vue({
  router,
  store,
  axios,
  vuetify,
  render: (h) => h(App)
}).$mount("#app");
