import axios from "../axios.js";

const MockAdapter = require("axios-mock-adapter");
const mock = new MockAdapter(axios);

export default mock;
