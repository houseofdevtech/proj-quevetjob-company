import Vue from "vue";
import VueRouter from "vue-router";

// import RegisterPersonal from "../views/RegisterPersonal.vue"

Vue.use(VueRouter);

const routes = [
  {
    path: "/registercompany",
    name: "RegisterCompany",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/RegisterCompany"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
  {
    path: "/managejob/:path",
    name: "ManageJob",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ManageJob"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
  {
    path: "/managejob",
    name: "ManageJob",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ManageJob"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
  {
    path: "/postjob/:path",
    name: "PostJob",

    component: () => import(/* webpackChunkName: "about" */ "../views/PostJob"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
  {
    path: "/postjob",
    name: "PostJob",

    component: () => import(/* webpackChunkName: "about" */ "../views/PostJob"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
  {
    path: "/registerpersonal",
    name: "RegisterPersonal",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/RegisterPersonal.vue"),
  },
  {
    path: "/registerpersonal/:path",
    name: "RegisterPersonal",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/RegisterPersonal.vue"),
  },
  {
    path: "/calendar",
    name: "Calendar",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Calendar.vue"),
  },

  {
    path: "/confirm-employment/:date",
    name: "ConfirmEmployment",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ConfirmEmployment.vue"),
  },

  {
    path: "/employeeDetailCancle",
    name: "EmployeeDetailCancle",

    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/EmployeeDetailCancle.vue"
      ),
  },
  {
    path: "/employeeDetails/:id",
    name: "EmployeeDetail",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/EmployeeDetail.vue"),
  },
  {
    path: "/employeeDetails/",
    name: "EmployeeDetail",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/EmployeeDetail.vue"),
  },
  {
    // path: "/employeeVote?id_line=:id_line&date_start=:date_start&date_end=:date_end&user_assessment_id=:user_assessment_id&type=:type/",
    path: "/employeeVote/",

    name: "EmployeeVote",

    props: true,

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/EmployeeVote.vue"),
  },
  {
    path: "/vote/",
    name: "Vote",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Vote.vue"),
  },

  {
    path: "/employeeDetailConfirm",
    name: "EmployeeDetailConfirm",

    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/EmployeeDetailConfirm.vue"
      ),
  },
  {
    path: "/employeeDetailNotCome",
    name: "EmployeeDetailNotCome",

    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/EmployeeDetailNotCome.vue"
      ),
  },

  {
    path: "/rule/:path",
    name: "Rule",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/Rule.vue"),
  },
  {
    path: "/registerpersonal2",
    name: "RegisterPersonal2",

    component: () => import("../views/RegisterPersonal2.vue"),
  },
  {
    path: "/registerpersonal2/:path",
    name: "RegisterPersonal2",

    component: () => import("../views/RegisterPersonal2.vue"),
  },

  {
    path: "/education",
    name: "Education",

    component: () => import("../views/Education.vue"),
  },
  {
    path: "/experience",
    name: "Experience",

    component: () => import("../views/Experience.vue"),
  },
  {
    path: "/usermanage",
    name: "UserManage",

    component: () => import("../views/UserManage.vue"),
  },
  {
    path: "/usermanage/:path",
    name: "UserManage",

    component: () => import("../views/UserManage.vue"),
  },
  {
    path: "/rule",
    name: "Rule",

    component: () => import("../views/Rule.vue"),
  },
  {
    path: "/addeducation",
    name: "addEducation",

    component: () => import("../views/addEducation.vue"),
  },
  {
    path: "/addexperience",
    name: "addExperience",

    component: () => import("../views/addExperience.vue"),
  },
  {
    path: "/detail",
    name: "Detail",

    component: () => import("../views/Detail.vue"),
  },
  {
    path: "/thankyou",
    name: "thankyou",

    component: () => import("../views/thankyou.vue"),
  },
  {
    path: "/thankyou_2",
    name: "thankyou_2",

    component: () => import("../views/thankyou_2.vue"),
  },
  {
    path: "/costdetail",
    name: "CostDetail",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/CostDetail"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
  {
    path: "/managepayment",
    name: "ManagePayment",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ManagePayment"),
    meta: {
      title: "สมัครสมาชิกพนักงาน",
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;
