export const liff = window.liff;
export const lineopen = (liffId) => {
  liff.openWindow({
    url: `https://liff.line.me/${liffId}`,
    external: false,
  });
};

export const init = (myLiffId) => {
  liff
    .init({
      liffId: myLiffId,
    })
    .then(() => {
      if (!liff.isLoggedIn() && !liff.isInClient()) {
        alert(
          'To get an access token, you need to be logged in. Please tap the "login" button below and try again.'
        );
        // liff.login();
      } else {
        const accessToken = liff.getAccessToken();

        console.log("accestoken :::" + accessToken);

        liff.getProfile().then(function(profile) {
          console.log("profile:::", profile.userId);
          localStorage.idLine = profile.userId;
          // export const getId = profile.userId;
        });

        console.log("local:::", localStorage.idLine);

        console.log("liff init success");
      }
    })
    .catch((err) => {
      console.log("liff init fail");
      console.log(err);
    });
};

export const vconsole = () => {
  window.vConsole = new window.VConsole({
    defaultPlugins: ["system", "network", "element", "storage"],
    maxLogNumber: 1000,
    onReady: function() {
      console.log("localstorage :::", localStorage.idLine);

      console.log("vConsole is ready.");
    },
    onClearLog: function() {
      console.log("on clearLog");
    },
  });
};

export const liffclose = () => {
  liff.closeWindow();
};
