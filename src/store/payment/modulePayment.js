import state from "./modulePaymentState";
import mutations from "./modulePaymentMutations";
import actions from "./modulePaymentActions";
import getters from "./modulePaymentGetter";
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
