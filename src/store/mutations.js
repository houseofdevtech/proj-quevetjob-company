import { baseUrl } from "../util/backend";

export const mutations = {
  addjobmutation(state, payload) {
    state.job = payload;
    console.log("state job", state.job);
  },
  addActivateAnn(state, payload) {
    state.announce = payload;
    console.log("announce", state.announce);
  },
  addAnnounceMutation(state, payload) {
    payload = JSON.parse(payload);
    state.announce.push(payload);
    console.log("state announce", state.announce);
  },
  deleteJobMutation(state, payload) {
    console.log(payload);
    // payload = JSON.parse(payload);

    let new_job = state.job.filter((e) => {
      console.log("e", e.idJob);

      console.log("payload in loop", payload.idJob);

      return e.idJob !== payload.idJob;
      // return;
    });
    console.log(new_job);

    state.job = new_job;

    console.log("state job delete", state.job);
  },
  addCompanyIdMutation(state, payload) {
    state.company_id = payload;
  },
  addSubCompanyIdMutation(state, payload) {
    state.sub_com_id = payload;
  },
  addressCompanyIdMutation(state, payload) {
    state.address_com_id = payload;
  },
  activeListMutation(state, payload) {
    state.list_Activate = payload;
    console.log(state.list_Activate);
  },
  addDetailEmMutation(state, payload) {
    state.detailEm = payload;
  },
  isPetShopMutation(state, payload) {
    state.petShop = payload;
  },
  datePaymentMutation(state, payload) {
    state.datePayment = payload;

    // console.log("mutation", state.datePayment);
  },
  addAnnounceUser(state, payload) {
    state.announce_user = payload;
    // console.log("state announce_user", state.announce_user);
  },
  addfull(state, payload) {
    state.full = payload;
  },
  addpart(state, payload) {
    state.part = payload;
  },
  price_vat(state, payload) {
    state.vat = payload;
    // console.log("vattt mutation", state.vat);
  },
  waitingFullMutation(state, payload) {
    // console.log("payload mutation", payload);
    payload.forEach(async (i) => {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      await fetch(
        `${baseUrl}api/image/profile/${i.user.member}`,

        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          result = JSON.parse(result);
          // console.log(result.link);
          if (result.link) {
            setTimeout(() => {
              Object.assign(i, { img: result.link });
              state.waitingFull = payload;
            }, 500);
          }
        })
        .catch((error) => console.log("error", error));
    });
  },
  waitingPartMutation(state, payload) {
    payload.forEach(async (i) => {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      await fetch(
        `${baseUrl}api/image/profile/${i.user.member}`,

        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          result = JSON.parse(result);
          // console.log(result.link);
          if (result.link) {
            setTimeout(() => {
              Object.assign(i, { img: result.link });
              state.waitingPart = payload;
              // console.log("state waiting", state.waiting);
            }, 500);
          }
        })
        .catch((error) => console.log("error", error));
    });
  },
  cancleMutation(state, payload) {
    payload.forEach(async (i) => {
      var requestOptions = {
        method: "GET",
        redirect: "follow",
      };

      await fetch(
        `${baseUrl}api/image/profile/${i.user.member}`,

        requestOptions
      )
        .then((response) => response.text())
        .then((result) => {
          result = JSON.parse(result);
          // console.log(result.link);
          if (result.link) {
            setTimeout(() => {
              Object.assign(i, { img: result.link });
              state.cancleAll = payload;
              // console.log("state cancle", state.cancleAll);
            }, 500);
          }
        })
        .catch((error) => console.log("error", error));
    });
  },
};
