import moment from "moment";
import lodash from "lodash";
import { baseUrl } from "../util/backend";
moment.locale("th");
export const actions = {
  async addjob({ commit, state }, payload) {
    // payload = JSON.parse(payload);
    // console.log(payload);

    let i = 0;
    let arr = [];
    let active = {
      level_1: [],
      level_2: [],
      level_3: [],
      level_4: [],
      level_5: [],
      level_6: [],
    };

    for (i; i < payload.length; i++) {
      if (
        payload[i].annoucement.status === "inactive" &&
        payload[i].status === "active"
      ) {
        payload[i].date_time_job.time_start = payload[
          i
        ].date_time_job.time_start.slice(0, -3);
        payload[i].date_time_job.time_end = payload[
          i
        ].date_time_job.time_end.slice(0, -3);
        payload[i].date_time_job.date_start = moment(
          payload[i].date_time_job.date_start
        ).format("dd DD-MM-YY");

        payload[i].date_time_job.date_end = moment(
          payload[i].date_time_job.date_end
        ).format("dd DD-MM-YY");

        // console.log(payload[i].date_time_job);

        arr.push(payload[i]);
      } else if (
        (payload[i].annoucement.status === "active" ||
          payload[i].annoucement.status === "full") &&
        payload[i].status === "active"
      ) {
        payload[i].date_time_job.time_start = payload[
          i
        ].date_time_job.time_start.slice(0, -3);
        payload[i].date_time_job.time_end = payload[
          i
        ].date_time_job.time_end.slice(0, -3);
        payload[i].date_time_job.date_start = moment(
          payload[i].date_time_job.date_start
        ).format("dd DD-MM-YY");
        payload[i].date_time_job.date_end = moment(
          payload[i].date_time_job.date_end
        ).format("dd DD-MM-YY");
        if (payload[i].level === 1) {
          active.level_1.push(payload[i]);
          // console.log("level 1", active.level_1);

          active.level_1.forEach(async function(o) {
            // console.log("find_id", o.job_id);
            let period = [];
            var req = {
              method: "GET",
              redirect: "follow",
            };

            await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                // console.log(result);
                result.forEach((i) => {
                  period.push({
                    day: moment(i.date_start).format("dd DD-MM-YY"),
                  });
                  o.period = period;
                });
                // console.log("level_1", active.level_1);
              })
              .catch((error) => console.log("error", error));
          });
        } else if (payload[i].level === 2) {
          active.level_2.push(payload[i]);
          // console.log("level 2", active.level_2);
          active.level_2.forEach(async function(o) {
            console.log("find_id", o.job_id);
            let period = [];
            var req = {
              method: "GET",
              redirect: "follow",
            };

            await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                // console.log(result);
                result.forEach((i) => {
                  period.push({
                    day: moment(i.date_start).format("dd DD-MM-YY"),
                  });
                  o.period = period;
                });
                // console.log("level_2", active.level_2);
              })
              .catch((error) => console.log("error", error));
          });
        } else if (payload[i].level === 3) {
          active.level_3.push(payload[i]);
          // console.log("level 3", active.level_3);
          active.level_3.forEach(async function(o) {
            console.log("find_id", o.job_id);
            let period = [];
            var req = {
              method: "GET",
              redirect: "follow",
            };
            await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                // console.log(result);
                result.forEach((i) => {
                  period.push({
                    day: moment(i.date_start).format("dd DD-MM-YY"),
                  });
                  o.period = period;
                });
                // console.log("level_3", active.level_3);
              })
              .catch((error) => console.log("error", error));
          });
        } else if (payload[i].level === 4) {
          active.level_4.push(payload[i]);
          // console.log("level 4", active.level_4);
          active.level_4.forEach(async function(o) {
            // console.log("find_id", o.job_id);
            let period = [];
            var req = {
              method: "GET",
              redirect: "follow",
            };

            await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                // console.log(result);
                result.forEach((i) => {
                  period.push({
                    day: moment(i.date_start).format("dd DD-MM-YY"),
                  });
                  o.period = period;
                });
                // console.log("level_4", active.level_4);
              })
              .catch((error) => console.log("error", error));
          });
        } else if (payload[i].level === 5) {
          active.level_5.push(payload[i]);
          // console.log("level 5", active.level_5);
          active.level_5.forEach(async function(o) {
            // console.log("find_id", o.job_id);
            let period = [];
            var req = {
              method: "GET",
              redirect: "follow",
            };

            await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                // console.log(result);
                result.forEach((i) => {
                  period.push({
                    day: moment(i.date_start).format("dd DD-MM-YY"),
                  });
                  o.period = period;
                });
                // console.log("level_5", active.level_5);
              })
              .catch((error) => console.log("error", error));
          });
        } else if (payload[i].level === 6) {
          active.level_6.push(payload[i]);
          // console.log("level 6", active.level_6);
          active.level_6.forEach(async function(o) {
            // console.log("find_id", o.job_id);
            let period = [];
            var req = {
              method: "GET",
              redirect: "follow",
            };

            await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
              .then((response) => response.text())
              .then((result) => {
                result = JSON.parse(result);
                // console.log(result);
                result.forEach((i) => {
                  period.push({
                    day: moment(i.date_start).format("dd DD-MM-YY"),
                  });
                  o.period = period;
                });
                // console.log("level_6", active.level_6);
              })
              .catch((error) => console.log("error", error));
          });
        }
      }
    }
    // console.log("active ", active);
    // console.log("arr job", arr);
    // let a = arr;
    arr.forEach(async function(o) {
      console.log("find_id", o.job_id);
      let period = [];
      var req = {
        method: "GET",
        redirect: "follow",
      };

      await fetch(`${baseUrl}api/job/period/${o.job_id}`, req)
        .then((response) => response.text())
        .then((result) => {
          result = JSON.parse(result);
          // console.log(result);
          result.forEach((i) => {
            period.push({
              day: moment(i.date_start).format("dd DD-MM-YY"),
            });
            o.period = period;
            state.period.obj.push(period);
          });

          // console.log("period", period);
        })
        .catch((error) => console.log("error", error));
    });

    // if (arr[0].period) {
    setTimeout(() => {
      commit("addjobmutation", arr);
      // console.log("arr for manage", arr[0]);
      commit("addActivateAnn", active);
    }, 500);

    // }
  },
  addAnnounceJob({ commit }, payload) {
    console.log(commit);
    console.log(payload);
  },
  deletejob({ commit }, payload) {
    commit("deleteJobMutation", payload);
  },
  addCompanyId({ commit }, payload) {
    commit("addCompanyIdMutation", payload);
  },
  addSubCompanyId({ commit }, payload) {
    commit("addSubCompanyIdMutation", payload);
  },
  addressCompanyId({ commit }, payload) {
    commit("addressCompanyIdMutation", payload);
  },
  addAllSelect({ commit, payload }) {
    commit("SET_ALL_SELECT", payload);
  },
  activeList({ commit }, payload) {
    commit("activeListMutation", payload);
  },
  addDetailEm({ commit }, payload) {
    commit("addDetailEmMutation", payload);
    // console.log(payload);
  },
  statusClassified({ commit, state }, payload) {
    let waiting = [];
    let cancle = [];
    let part_waiting = [];

    console.log("status action", payload);
    let count_full = 0;
    let count_part = 0;
    payload.deal_job.forEach(async (o) => {
      o.annoucement.date_time_job.date_start = moment(
        o.annoucement.date_time_job.date_start
      ).format("dd DD-MM-YY");
      o.annoucement.date_time_job.date_end = moment(
        o.annoucement.date_time_job.date_end
      ).format("dd DD-MM-YY");
      o.annoucement.date_time_job.time_start = o.annoucement.date_time_job.time_start.slice(
        0,
        -3
      );
      o.annoucement.date_time_job.time_end = o.annoucement.date_time_job.time_end.slice(
        0,
        -3
      );
      o.annoucement.deal_jobs.forEach((p) => {
        console.log("ppp");

        Object.assign(p, {
          date_time_job: o.annoucement.date_time_job,
          level: o.level,
          position: o.position,
          type: o.type,
        });
        waiting.push(p);
        if (
          p.status !== "confirm" &&
          p.status !== "complete" &&
          p.status !== "waiting"
        ) {
          cancle.push(p);
        }
      });

      waiting = lodash.filter(waiting, (i) => {
        return i.status == "waiting";
      });

      commit("waitingFullMutation", waiting);

      // console.log("waiting action", waiting);

      count_full = waiting.length;
    });
    payload.deal_job_part_time.forEach(async (o) => {
      o.annoucement.deal_job_part_times.forEach((p) => {
        // console.log("ppp", p);
        p.period_job.date_start = moment(p.period_job.date_start).format(
          "dd DD-MM-YY"
        );
        p.period_job.date_end = moment(p.period_job.date_end).format(
          "dd DD-MM-YY"
        );
        p.period_job.time_start = p.period_job.time_start.slice(0, -3);
        p.period_job.time_end = p.period_job.time_end.slice(0, -3);
        Object.assign(p, {
          date_time_job: o.annoucement.date_time_job,
          level: o.level,
          position: o.position,
          type: o.type,
        });

        part_waiting.push(p);
        if (
          p.status !== "confirm" &&
          p.status !== "complete" &&
          p.status !== "waiting"
        ) {
          cancle.push(p);
        }
      });
      part_waiting = lodash.filter(part_waiting, (i) => {
        return i.status == "waiting";
      });
      commit("waitingPartMutation", part_waiting);

      count_part = part_waiting.length;
    });
    state.cancleCount = cancle.length;
    state.waitingCount = count_full + count_part;
    commit("cancleMutation", cancle);

    // console.log("cancle action", cancle);
  },
};
