import axios from "@/axios.js";
import lodash from "lodash";

export default {
  timePickerSelectedEvent({ commit }, event) {
    console.table([event]);
    commit("TIME_PICKER_SELECTED_EVENT", event);
  },
  partTimeCalendarSelectedEvent({ commit }, event) {
    let start_end = {
      first: null,
      last: null,
    };
    console.log("action", event);
    let o;
    // let a;
    o = lodash.sortBy(event, [
      function(o) {
        return o;
      },
    ]);
    console.log("o sort", o);
    let period = [];

    lodash.forEach(o, function(a) {
      let period_detail = {
        date_start: null,
        date_end: null,
      };
      period_detail.date_start = a;
      period_detail.date_end = a;
      period.push(period_detail);
      console.log("period array", period);

      return null;
    });

    start_end.first = lodash.head(o);
    start_end.last = lodash.last(o);

    console.log("first", start_end);

    commit("SET_PART_TIME_CALENDAR", o);
    commit("SET_PERIOD_TIME", period);

    commit("SET_START_END", start_end);
  },
  fullTimeCalendarFromDate({ commit }, event) {
    commit("SET_FULL_TIME_FROM_DATE", event);
  },
  fullTimeCalendarToDate({ commit }, event) {
    commit("SET_FULL_TIME_TO_DATE", event);
  },
  fullTimeCalendarAllDate({ commit }, event) {
    let start_end = {
      first: null,
      last: null,
    };
    console.log("event", event);
    let o;
    // let a;
    o = lodash.sortBy(event, [
      function(o) {
        return o;
      },
    ]);
    console.log(o);
    let period = [];
    lodash.forEach(o, function(a) {
      let period_detail = {
        date_start: null,
        date_end: null,
      };
      period_detail.date_start = a;
      period_detail.date_end = a;
      period.push(period_detail);
      console.log("period array", period);

      return null;
    });

    start_end.first = lodash.head(o);
    start_end.last = lodash.last(o);

    console.log("first", start_end);

    commit("SET_PART_TIME_CALENDAR", o);
    commit("SET_PERIOD_TIME", period);

    commit("SET_START_END", start_end);

    commit("SET_FULL_TIME_ALL_DATE", event);
  },
  addEvent({ commit }, event) {
    return new Promise((resolve, reject) => {
      axios
        .post("/api/apps/calendar/events/", { event })
        .then((response) => {
          commit("ADD_EVENT", Object.assign(event, { id: response.data.id }));
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  fetchEvents({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get("/api/apps/calendar/events")
        .then((response) => {
          commit("SET_EVENTS", response.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  fetchEventLabels({ commit }) {
    return new Promise((resolve, reject) => {
      axios
        .get("/api/apps/calendar/labels")
        .then((response) => {
          commit("SET_LABELS", response.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  editEvent({ commit }, event) {
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/apps/calendar/event/${event.id}`, { event })
        .then((response) => {
          // Convert Date String to Date Object
          const event = response.data;
          event.startDate = new Date(event.startDate);
          event.endDate = new Date(event.endDate);

          commit("UPDATE_EVENT", event);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  removeEvent({ commit }, eventId) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`/api/apps/calendar/event/${eventId}`)
        .then((response) => {
          commit("REMOVE_EVENT", response.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  eventDragged({ commit }, payload) {
    return new Promise((resolve, reject) => {
      axios
        .post(`/api/apps/calendar/event/dragged/${payload.event.id}`, {
          payload,
        })
        .then((response) => {
          // Convert Date String to Date Object
          const event = response.data;
          event.startDate = new Date(event.startDate);
          event.endDate = new Date(event.endDate);

          commit("UPDATE_EVENT", event);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  updateJobAction({ commit }, payload) {
    console.log("Action update payload", payload);

    commit("updateJobMutation", payload);
  },
};
