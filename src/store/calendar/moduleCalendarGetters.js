export default {
  getPostJobTimer: (state) => state.postJobTimePicker,
  getPartTimeDaySelected: (state) => state.partTimeSelected,
  getFullTimeFromDate: (state) => state.fullTimeFromDate,
  getFullTimeToDate: (state) => state.fullTimeToDate,
  getEvent: (state) => (eventId) =>
    state.events.find((event) => event.id === eventId),
  getPeriodArr: (state) => state.period_array,
  getStartEnd: (state) => state.start_end,
  getAllDateSeleceted: (state) => state.allDate,
  getUpdateJob: (state) => state.updateJob,
};
