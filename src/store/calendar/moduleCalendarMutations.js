export default {
  ADD_EVENT(state, event) {
    state.events.push(event);
  },
  TIME_PICKER_SELECTED_EVENT(state, event) {
    state.postJobTimePicker = event;
  },
  SET_EVENTS(state, events) {
    state.events = events;
  },
  SET_LABELS(state, labels) {
    state.eventLabels = labels;
  },
  SET_FULL_TIME_FROM_DATE(state, event) {
    state.fullTimeFromDate = event;
  },
  SET_FULL_TIME_TO_DATE(state, event) {
    state.fullTimeToDate = event;
  },
  UPDATE_EVENT(state, event) {
    const eventIndex = state.events.findIndex((e) => e.id === event.id);
    Object.assign(state.events[eventIndex], event);
  },
  REMOVE_EVENT(state, eventId) {
    const eventIndex = state.events.findIndex((e) => e.id === eventId);
    state.events.splice(eventIndex, 1);
  },
  SET_PART_TIME_CALENDAR(state, daySelected) {
    state.partTimeSelected = daySelected;
  },
  SET_FULL_TIME_ALL_DATE(state, payload) {
    state.allDate = payload;
  },
  SET_PERIOD_TIME(state, payload) {
    state.period_array = payload;
  },
  SET_START_END(state, payload) {
    state.start_end = payload;
  },
  updateJobMutation(state, payload) {
    state.updateJob = payload;
  },
};
