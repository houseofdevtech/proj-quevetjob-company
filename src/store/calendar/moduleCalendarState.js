export default {
  eventLabels: [],
  events: [],
  partTimeSelected: [],
  postJobTimePicker: {
    tStart: "00",
    tEnd: "00",
  },
  fullTimeFromDate: "",
  fullTimeToDate: "",
  period_array: [],
  start_end: {},
  allDate: {},
  updateJob: {},
};
