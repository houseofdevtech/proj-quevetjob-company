import Vue from "vue";
import Vuex from "vuex";
import { state } from "./state";
import { actions } from "./actions";
import { mutations } from "./mutations";
import { getters } from "./getters";

Vue.use(Vuex);
import moduleCalendar from "./calendar/moduleCalendar";
import modulePayment from "./payment/modulePayment";
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: {
    calendar: moduleCalendar,
    payment: modulePayment,
  },
});
